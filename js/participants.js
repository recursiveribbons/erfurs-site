(function loadParticipants() {
    fetch("https://api.erfurs.de/participants")
        .then(response => response.json())
        .then(result => setParticipants(result))
        .catch(() => setError());
})();

function setParticipants(participants) {
    const list = document.getElementById("participants");
    if (participants.length === 0) {
        const element = document.createElement("li");
        element.innerText = "Keine Teilnehmer";
        element.className = "list-group-item";
        list.appendChild(element);
    } else {
        participants.forEach(participant => {
            const element = document.createElement("li");
            element.innerText = formatPerson(participant);
            element.className = "list-group-item";
            list.appendChild(element);
        });
    }
}

function setError() {
    const list = document.getElementById("participants");
    const element = document.createElement("li");
    element.innerText = "Error, Liste konnte nicht geladen werden";
    element.className = "list-group-item";
    list.appendChild(element);
}

function formatPerson(person) {
    let result = person.name;
    result += person.arrivalTime !== 4 ? " (" + formatTime(person.arrivalTime) + ")" : "";
    result += person.meal ? ", essen" : "";
    result += person.stay ? ", übernachten" : "";
    result += person.pet ? ", tier - " + person.pet : "";
    return result;
}

function formatTime(time) {
    return (time + 12) + ":00";
}
